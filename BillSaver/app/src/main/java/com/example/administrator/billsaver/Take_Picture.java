package com.example.administrator.billsaver;

import android.app.ProgressDialog;
import
        android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.net.Uri;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

/**
 * Created by H TAHIR AZIZ on 8/25/2018.
 */

public class Take_Picture extends AppCompatActivity {
    private Button takePicture, savePicture;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    private String mCurrentPhotoPath;
    private StorageReference mStorageRef;
    private ProgressDialog mProgress;
    private Uri file;
    private DatabaseReference mDatabase,mDatabase2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_picture);
        takePicture = (Button) findViewById(R.id.take_picture_btn);
        savePicture = (Button) findViewById(R.id.save_picture_btn);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mProgress=new ProgressDialog(this);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Bill");

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent,REQUEST_IMAGE_CAPTURE);
                }
                //dispatchTakePictureIntent();


                }
        });
        savePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent intent = new Intent(Take_Picture.this, User_Login.class);
                startActivity(intent);
                finish();
                return;
            }
        });
    }




    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        mProgress.setMessage("Picture uploading in progress-----");
        mProgress.show();

//get the camera image
        Bundle extras = data.getExtras();
        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] dataBAOS = baos.toByteArray();
        StorageReference storageRef = FirebaseStorage.getInstance().getReferenceFromUrl("gs://billsaver-6f030.appspot.com/photos");
        StorageReference imagesRef = storageRef.child("filename" + new Date().getTime());
        UploadTask uploadTask = imagesRef.putBytes(dataBAOS);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(getApplicationContext(),"Sending failed"+exception.getMessage(), Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();


                SharedPreferences SharedPreferences = PreferenceManager

                        .getDefaultSharedPreferences(getApplicationContext());

                String ref = SharedPreferences.getString("Existing_Bill_Reference", "");
                String item = SharedPreferences.getString("Current_Item", "");
                String key1=SharedPreferences.getString("Current_Item_Key", "");
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                String strDate = dateFormat.format(date).toString();


                HashMap<String,String> dataMap=new HashMap<String,String>();
                dataMap.put("Picture_Url",downloadUrl.toString());
                dataMap.put("Bill_Date",strDate);
                mDatabase2=mDatabase.child(ref);
                mDatabase2.child(key1).setValue(dataMap);
                Toast.makeText(getApplicationContext(),"Sending Succeed", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
            }
        });


    }
}




