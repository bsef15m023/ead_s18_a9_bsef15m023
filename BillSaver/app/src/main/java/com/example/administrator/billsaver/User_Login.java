package com.example.administrator.billsaver;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

/**
 * Created by H TAHIR AZIZ on 8/25/2018.
 */

public class User_Login extends AppCompatActivity {

    private Button generateNewBill,Logout;
    private DatabaseReference mDatabase;
    private String key;
// ...



    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        generateNewBill=(Button)findViewById(R.id.generate_new_bill);
        Logout=(Button)findViewById(R.id.Logout);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Bill");
        generateNewBill.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {


                String dataMap="New Bill";
                key = mDatabase.push().getKey();
                mDatabase.child(key).setValue(dataMap);

                SharedPreferences SharedPreferences = PreferenceManager

                        .getDefaultSharedPreferences(getApplicationContext());

                SharedPreferences.Editor editor = SharedPreferences.edit();

                editor.putString("Existing_Bill_Reference", key);

                editor.commit();

                Intent intent = new Intent(User_Login.this, User_Home.class);
                startActivity(intent);
                finish();
                return;

            }
        });
        Logout.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v)
            {
                Intent intent = new Intent(User_Login.this, MainActivity.class);
                startActivity(intent);
                finish();
                return;
            }
        });
    }
}
