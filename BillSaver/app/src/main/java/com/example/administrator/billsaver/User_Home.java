package com.example.administrator.billsaver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class User_Home extends AppCompatActivity {

    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    private Button saveBill;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private String key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__home);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mDatabase= FirebaseDatabase.getInstance().getReference().child("Bill");

        mAdapter = new MoviesAdapter(movieList);

        recyclerView.setHasFixedSize(true);
        saveBill=(Button)findViewById(R.id.Save_Bill);
        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Movie movie = movieList.get(position);
                Intent intent = new Intent(User_Home.this, Item_details.class);
                HashMap<String,String> dataMap=new HashMap<String,String>();
                dataMap.put("Item_Name",movie.getTxt());
                 SharedPreferences SharedPreferences = PreferenceManager

                        .getDefaultSharedPreferences(getApplicationContext());

                String ref = SharedPreferences.getString("Existing_Bill_Reference", "");
                key = mDatabase.child(ref).push().getKey();
                //mDatabase.child(ref).child(key).setValue(dataMap);




                SharedPreferences = PreferenceManager

                        .getDefaultSharedPreferences(getApplicationContext());

                SharedPreferences.Editor editor = SharedPreferences.edit();

                editor.putString("Current_Item_Key", key);
                editor.putString("Current_Item", movie.getTxt());

                editor.commit();

                editor.commit();
                startActivity(intent);
                finish();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        saveBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences SharedPreferences = PreferenceManager

                        .getDefaultSharedPreferences(getApplicationContext());


                String ref = SharedPreferences.getString("Existing_Bill_Reference", "");
                key = mDatabase.child(ref).push().getKey();
                //mDatabase.child(ref).child(key).setValue(dataMap);




                SharedPreferences = PreferenceManager

                        .getDefaultSharedPreferences(getApplicationContext());

                SharedPreferences.Editor editor = SharedPreferences.edit();

                editor.putString("Current_Item_Key", key);

                editor.commit();


                Intent intent = new Intent(User_Home.this, Take_Picture.class);
                startActivity(intent);
                finish();
                return;
            }
        });

        prepareSampleMovieData();
    }
    private void prepareSampleMovieData() {
        Movie movie = new Movie(R.drawable.ax,"AX");
        movieList.add(movie);

        movie = new Movie(R.drawable.axe,"AXE");
        movieList.add(movie);

        movie = new Movie(R.drawable.bolster,"BOLSTER");
        movieList.add(movie);

        movie = new Movie(R.drawable.ladder,"Ladder");
        movieList.add(movie);

        movie = new Movie(R.drawable.plier,"PLIER");
        movieList.add(movie);

        movie = new Movie(R.drawable.ruler,"RULER");
        movieList.add(movie);

        movie = new Movie(R.drawable.saw,"SAW");
        movieList.add(movie);

        movie = new Movie(R.drawable.screw,"SCREW");
        movieList.add(movie);

        movie = new Movie(R.drawable.shovel,"SHOVEL");
        movieList.add(movie);

        movie = new Movie(R.drawable.blank,"ANY OTHER");
        movieList.add(movie);

        movie = new Movie(R.drawable.blank," ");
        movieList.add(movie);

        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();
    }
}
