package com.example.administrator.billsaver;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends Activity {
    private EditText login, password;
    private Button login_btn, sign_up;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);

        login_btn = (Button) findViewById(R.id.login_btn);
        sign_up = (Button) findViewById(R.id.sign_up);
        mAuth = FirebaseAuth.getInstance();

        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, sign_up.class);
                startActivity(intent);
                finish();
                return;

            }
        });
        login_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (login.getText().toString().equals("") || password.getText().toString().equals("") ) {
                    Toast.makeText(getApplicationContext(), "Fill all the fields completely", Toast.LENGTH_LONG).show();
                    return;
                }
                mAuth.signInWithEmailAndPassword(login.getText().toString().trim(), password.getText().toString().trim())
                        .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    Intent intent = new Intent(MainActivity.this, User_Login.class);
                                    startActivity(intent);
                                    finish();
                ;                    FirebaseUser user = mAuth.getCurrentUser();
                                    return;


                                } else {
                                    Toast.makeText(getApplicationContext(), "Invalid UserName or Password", Toast.LENGTH_LONG).show();
                 ;               }

                            }
        });

    }
});
    }
}