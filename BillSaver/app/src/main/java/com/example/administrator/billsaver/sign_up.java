package com.example.administrator.billsaver;

import
        android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class sign_up extends AppCompatActivity {
    private Button sign_up;
    private EditText Login,Password,verifyPassword;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sign_up = (Button) findViewById(R.id.new_sign_up_btn);
        Login=(EditText)findViewById(R.id.new_login);
        Password=(EditText)findViewById(R.id.new_password);
        verifyPassword=(EditText)findViewById(R.id.new_password1);
        mAuth = FirebaseAuth.getInstance();

        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Login.getText().toString().equals("")||Password.getText().toString().equals("")||verifyPassword.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Fill all the fields completely",Toast.LENGTH_LONG).show();
                    return;
                }
                if(!verifyPassword.getText().toString().equals(Password.getText().toString()))
                {
                    Toast.makeText(getApplicationContext(),"Your password not match with verify Password",Toast.LENGTH_LONG).show();
                    return;
                }
                String login=Login.getText().toString().trim();
                password=Password.getText().toString().trim();
                mAuth.createUserWithEmailAndPassword(login, password)
                        .addOnCompleteListener(sign_up.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    Intent intent = new Intent(sign_up.this,User_Login.class);
                                    startActivity(intent);
                                    finish();
                                    return;

                                } else {
                                    if(password.length()<6){
                                        Toast.makeText(getApplicationContext(),"Password should be eigth digit",Toast.LENGTH_LONG).show();
                                    }
                                    else {
                                        Toast.makeText(getApplicationContext(), "Authenticiation Failed", Toast.LENGTH_LONG).show();
                                    }
                                }


                            }
                        });
            }
        });
    }
}
