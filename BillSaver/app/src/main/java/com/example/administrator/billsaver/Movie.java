package com.example.administrator.billsaver;

/**
 * Created by H TAHIR AZIZ on 8/24/2018.
 */

public class Movie {
    public int imageId;
    public String txt;

    Movie( int imageId, String text) {

        this.imageId = imageId;
        this.txt=text;
    }
    public int getImageId() {
        return imageId;
    }
    public String getTxt() {
        return  txt;
    }
}


