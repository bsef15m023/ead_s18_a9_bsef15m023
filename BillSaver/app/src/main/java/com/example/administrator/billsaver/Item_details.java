package com.example.administrator.billsaver;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

/**
 * Created by H TAHIR AZIZ on 8/28/2018.
 */

public class Item_details extends AppCompatActivity{
    private Button save;
    private EditText price,quantity,description;
    String Description,key1;
    int Price,Quantity;
    private DatabaseReference mDatabase,mDatabase2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        save = (Button) findViewById(R.id.Save_Bill1);
        price=(EditText)findViewById(R.id.Price);
        quantity=(EditText)findViewById(R.id.Quantity);
        description=(EditText)findViewById(R.id.description);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Bill");

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(price.getText().toString().equals("")||quantity.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Fill All the fields completely",Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    Price = Integer.parseInt(price.getText().toString());
                    Quantity = Integer.parseInt(quantity.getText().toString());
                }
                catch(NumberFormatException e)
                {
                    Toast.makeText(getApplicationContext(),"Enter true format of price and quantity",Toast.LENGTH_LONG).show();
                    return;
                }
                Description="";
                Description=description.getText().toString();

                SharedPreferences SharedPreferences = PreferenceManager

                        .getDefaultSharedPreferences(getApplicationContext());

                String ref = SharedPreferences.getString("Existing_Bill_Reference", "");
                String item = SharedPreferences.getString("Current_Item", "");
                key1=SharedPreferences.getString("Current_Item_Key", "");

                Toast.makeText(getApplicationContext(),"Total price= "+(Price*Quantity), Toast.LENGTH_SHORT).show();

                HashMap<String,String> dataMap=new HashMap<String,String>();
                dataMap.put("Price",price.getText().toString());
                dataMap.put("Qunatity",quantity.getText().toString());
                dataMap.put("Description",Description);
                dataMap.put("Item_Name",item);
                mDatabase2=mDatabase.child(ref);
                mDatabase2.child(key1).setValue(dataMap);


                Intent intent = new Intent(Item_details.this,User_Home.class);
                startActivity(intent);
                finish();
                return;
            }
        });
    }
}
